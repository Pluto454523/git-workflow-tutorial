package main

import (
	"fmt"

	"gitlab.com/Pluto454523/git-workflow-tutorial/src/repository/order_repository"
	productrepository "gitlab.com/Pluto454523/git-workflow-tutorial/src/repository/product_repository"
	taskrepository "gitlab.com/Pluto454523/git-workflow-tutorial/src/repository/task_repository"
	"gitlab.com/Pluto454523/git-workflow-tutorial/src/repository/user_repository"

	"gitlab.com/Pluto454523/git-workflow-tutorial/src/usecase"
)

func main() {

	userRepo := user_repository.NewUserRepository()
	orderRepo := order_repository.NewOrderRepository()
	productRepo := productrepository.NewProductRepository()
	taskRepo := taskrepository.NewTaskRepository()
	usecase := usecase.NewUsecase(userRepo, orderRepo, productRepo, taskRepo)
	fmt.Printf("%#v\n", usecase.LoginByID(1))
	fmt.Printf("%#v\n", usecase.MockOrderData())

}
