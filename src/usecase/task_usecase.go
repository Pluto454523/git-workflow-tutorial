package usecase

import "gitlab.com/Pluto454523/git-workflow-tutorial/src/entity"

func (uc usecases) CreateTask(id uint32, title string) entity.Task {

	task := uc.taskRepo.CreateTask(id, title)
	return task
}
