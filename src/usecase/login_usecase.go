package usecase

import "gitlab.com/Pluto454523/git-workflow-tutorial/src/entity"

func (uc usecases) LoginByID(id uint32) entity.UserEntity {

	user := uc.userRepo.GetUserByID(id)
	return user
}
