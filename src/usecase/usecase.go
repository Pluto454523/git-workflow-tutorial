package usecase

import "gitlab.com/Pluto454523/git-workflow-tutorial/src/usecase/repository"

type usecases struct {
	userRepo    repository.UserRepository
	orderRepo   repository.OrderRepository
	productRepo repository.ProductRepository
	taskRepo    repository.TaskRepository
}

func NewUsecase(
	userRepo repository.UserRepository,
	orderRepo repository.OrderRepository,
	productRepo repository.ProductRepository,
	taskRepo repository.TaskRepository,
) *usecases {
	return &usecases{
		userRepo:    userRepo,
		orderRepo:   orderRepo,
		productRepo: productRepo,
		taskRepo:    taskRepo,
	}
}
