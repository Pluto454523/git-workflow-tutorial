package repository

import "gitlab.com/Pluto454523/git-workflow-tutorial/src/entity"

type UserRepository interface {
	GetUserByID(id uint32) entity.UserEntity
}

type OrderRepository interface {
	MockOrderData() entity.Order
}
type ProductRepository interface {
	CreateProduct(id uint32, name string, price float64) entity.Product
}
type TaskRepository interface {
	CreateTask(id uint32, title string) entity.Task
}
