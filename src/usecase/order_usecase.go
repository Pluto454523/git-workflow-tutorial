package usecase

import "gitlab.com/Pluto454523/git-workflow-tutorial/src/entity"

func (uc usecases) MockOrderData() entity.Order {

	order := uc.orderRepo.MockOrderData()

	return order
}
