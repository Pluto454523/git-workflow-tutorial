package usecase

import "gitlab.com/Pluto454523/git-workflow-tutorial/src/entity"

func (uc usecases) CreateProduct(id uint32, name string, price float64) entity.Product {

	product := uc.productRepo.CreateProduct(id, name, price)
	return product
}
