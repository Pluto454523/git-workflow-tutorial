package entity

type Product struct {
	Id    uint32
	Name  string
	Price float64
}
