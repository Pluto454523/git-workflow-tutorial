package entity

import (
	"time"
)

type Order struct {
	OrderId      string
	OrderNumber  string
	ProviderCode string
	StoreId      string
	Items        []Item
	State        string
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

type Item struct {
	Price     float32
	Qty       int
	ProductId string
}
