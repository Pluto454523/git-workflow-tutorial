package entity

type Task struct {
	Id    uint32
	Title string
}
