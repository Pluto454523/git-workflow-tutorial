package taskrepository

import (
	"gitlab.com/Pluto454523/git-workflow-tutorial/src/entity"
	"gitlab.com/Pluto454523/git-workflow-tutorial/src/usecase/repository"
)

type TaskRepositorySchema struct {
	Id    uint32
	Title string
}

type TaskRepositoryDependency struct {
}

func (repo TaskRepositoryDependency) CreateTask(id uint32, title string) entity.Task {

	task := entity.Task{

		Id:    id,
		Title: title,
	}

	return task
}

func NewTaskRepository() repository.TaskRepository {

	return &TaskRepositoryDependency{}
}
