package order_repository

import (
	"time"

	"gitlab.com/Pluto454523/git-workflow-tutorial/src/entity"
	"gitlab.com/Pluto454523/git-workflow-tutorial/src/usecase/repository"
)

type OrderRepositoryModel struct {
	OrderId      string
	OrderNumber  string
	ProviderCode string
	StoreId      string
	Items        []ItemRepositoryModel
	State        string
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

type ItemRepositoryModel struct {
	OrderId   uint
	Price     string
	Qty       int
	ProductId string
}

type OrderRepositoryDB struct {
}

func (repo OrderRepositoryDB) MockOrderData() entity.Order {
	return entity.Order{
		OrderId:      "123456",
		OrderNumber:  "ORD-2024-001",
		ProviderCode: "PROV-01",
		StoreId:      "STORE-01",
		State:        "Pending",
		CreatedAt:    time.Now().Add(-24 * time.Hour),
		UpdatedAt:    time.Now(),
		Items: []entity.Item{
			{
				Price:     100.00,
				Qty:       2,
				ProductId: "PROD-01",
			},
			{
				Price:     200.00,
				Qty:       1,
				ProductId: "PROD-02",
			},
		},
	}
}

func NewOrderRepository() repository.OrderRepository {
	return &OrderRepositoryDB{}
}
