package user_repository

import (
	"gitlab.com/Pluto454523/git-workflow-tutorial/src/entity"
	"gitlab.com/Pluto454523/git-workflow-tutorial/src/usecase/repository"
)

type UserRepositorySchema struct {
	ID   uint32
	Name string
}

type UserRepositoryDependency struct {
}

func (repo UserRepositoryDependency) GetUserByID(id uint32) entity.UserEntity {

	users := []entity.UserEntity{
		{
			ID:   1,
			Name: "Nawin",
		},
	}

	for i := range users {
		if users[i].ID == id {
			return users[i]
		}
	}

	return entity.UserEntity{}
}

func NewUserRepository() repository.UserRepository {
	return &UserRepositoryDependency{}
}
