package productrepository

import (
	"gitlab.com/Pluto454523/git-workflow-tutorial/src/entity"
	"gitlab.com/Pluto454523/git-workflow-tutorial/src/usecase/repository"
)

type ProductRepositorySchema struct {
	Id    uint32
	Name  string
	Price float64
}

type ProductRepositoryDependency struct {
}

func (repo ProductRepositoryDependency) CreateProduct(id uint32, name string, price float64) entity.Product {

	product := entity.Product{

		Id:    id,
		Name:  name,
		Price: price,
	}

	return product
}

func NewProductRepository() repository.ProductRepository {
	return &ProductRepositoryDependency{}
}
